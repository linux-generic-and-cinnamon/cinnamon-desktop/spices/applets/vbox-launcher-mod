# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-09-13 13:59+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: applet.js:37
msgid "This image is disabled due to missing files.\n"
msgstr ""

#: applet.js:126
msgid "Settings"
msgstr ""

#: applet.js:154
msgid "Display"
msgstr ""

#: applet.js:155
msgid "Headless"
msgstr ""

#: applet.js:190
msgid "ERROR. No compatible virtual machine programs found."
msgstr ""

#: applet.js:312
msgid "[ Update list ]"
msgstr ""

#. metadata.json->name
msgid "Vbox Launcher Mod"
msgstr ""

#. metadata.json->description
msgid "An applet to launch virtual machines."
msgstr ""

#. settings-schema.json->autoUpdate->description
msgid "Auto update (slow)"
msgstr ""

#. settings-schema.json->showHeadlessMode->description
msgid "Show Headless Mode"
msgstr ""
